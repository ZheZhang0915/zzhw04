﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Threading;

namespace _5092_homework4
{
    public partial class Form1 : Form
    {
        double[] price = new double[10];
        double[] pricer = new double[4];
        System.Diagnostics.Stopwatch watch = new System.Diagnostics.Stopwatch();
        public delegate void Getresult();
        public Getresult myresult;


        public Form1()
        {
            InitializeComponent();
            myresult = new Getresult(Getresult_method);
        }
        public  void Update(int i) 
        {
            if (InvokeRequired)
            {
                this.BeginInvoke(new Action<int>(Update), new object[] { i });
                return;
            }
            progressBar1.Value = i;
        }
        public void Getresult_method()
        {
            int cores = Environment.ProcessorCount;
            cores_n.Text = Convert.ToString(cores);
            if (radioButton1.Checked == true)
            {
                price1.Text = Convert.ToString(pricer[0]);
                delta1.Text = Convert.ToString(price[0]);
                gamma1.Text = Convert.ToString(price[1]);
                theta1.Text = Convert.ToString(price[2]);
                rho1.Text = Convert.ToString(price[3]);
                vega1.Text = Convert.ToString(price[4]);
                se1.Text = Convert.ToString(pricer[1]);
                
            }
            else
            {
                price1.Text = Convert.ToString(pricer[2]);
                delta1.Text = Convert.ToString(price[5]);
                gamma1.Text = Convert.ToString(price[6]);
                theta1.Text = Convert.ToString(price[7]);
                rho1.Text = Convert.ToString(price[8]);
                vega1.Text = Convert.ToString(price[9]);
                se1.Text = Convert.ToString(pricer[3]);
            }
            watch.Stop();
            //Update(100);
            runTime.Text = watch.Elapsed.Minutes.ToString() + " minutes "
                + watch.Elapsed.Seconds.ToString() + " seconds " + watch.Elapsed.Milliseconds.ToString() + " milliseconds ";
            
        }

        private void label9_Click(object sender, EventArgs e)
        {

        }

        private void Form1_Load(object sender, EventArgs e)
        {

        }

        private void label24_Click(object sender, EventArgs e)
        {

        }

        private void label25_Click(object sender, EventArgs e)
        {

        }

        public static double[,] ran;
        private void button1_Click(object sender, EventArgs e)
        {
            
            watch.Reset();
            watch.Start();
            int trials = Convert.ToInt32(Trials.Text);
            int N = Convert.ToInt32(steps.Text);
            double S = Convert.ToDouble(Stockprice.Text);
            double K = Convert.ToDouble(Kprice.Text);
            double r = Convert.ToDouble(rate.Text);
            double T = Convert.ToDouble(Tenor.Text);
            double sigma = Convert.ToDouble(vol.Text);
            ran = new double[trials, N];
            bool anti, cv, muti;
            if (antithetic.Checked)
            {
                 anti = true;
            }
            else
            {
                anti = false;
            }
            if (delta_c.Checked)
            {
                cv = true;
            }
            else
            {
                cv = false;
            }
            if (mutithread.Checked)
            {
                muti = true;
            }
            else
            {
                muti = false;
            }
            Update(5);

            Action<object> cal = x =>
            {
                Class1 sim = new Class1(trials, N);
                if (muti)
                {
                    sim.multithread();
                }
                else
                {
                    sim.randomnum();
                }
                Update(10);
                EuropeanOption option1 = new EuropeanOption();
                pricer = option1.GetPrice(trials, N, S, K, r, T, sigma, ran, anti, cv, muti);Update(15);
                double[] Price1 = option1.GetPrice(trials, N, S, K, r, T, sigma, ran, anti, cv, muti); Update(20);
                double[] Delta1 = option1.GetPrice(trials, N,1.001* S, K, r, T, sigma, ran, anti, cv, muti); Update(40);
                double[] Gmma1 = option1.GetPrice(trials, N,0.999* S, K, r, T, sigma, ran, anti, cv, muti); Update(60);
                double[] Theta1 = option1.GetPrice(trials, N, S, K, r, 1.001*T, sigma, ran, anti, cv, muti); Update(80);
                double[] Rho1 = option1.GetPrice(trials, N, S, K, 1.0001 * r, T, sigma, ran, anti,cv, muti); Update(100);
                double[] Vega1 = option1.GetPrice(trials, N, S, K, r, T, 1.0001 * sigma, ran, anti, cv, muti); 
                double callD = (Delta1[0] - Price1[0]) / (0.001 * S);
                double callG = (Gmma1[0] + Delta1[0] - 2 * Price1[0]) / (0.001 * 0.001 * S * S);
                double callT = -(Theta1[0] - Price1[0]) / (0.001 * T);
                double callR = (Rho1[0] - Price1[0]) / (0.0001 * r);
                double callV = (Vega1[0] - Price1[0]) / (0.0001 * sigma);
                double putD = (Delta1[2] - Price1[2]) / (0.001 * S);
                double putG = (Gmma1[2] + Delta1[2] - 2 * Price1[2]) / (0.001 * 0.001 * S * S);
                double putT = -(Theta1[2] - Price1[2]) / (0.001 * T);
                double putR = (Rho1[2] - Price1[2]) / (0.0001 * r);
                double putV = (Vega1[2] - Price1[2]) / (0.0001 * sigma);
                price[0] = callD;
                price[1] = callG;
                price[2] = callT;
                price[3] = callR;
                price[4] = callV;

                price[5] = putD;
                price[6] = putG;
                price[7] = putT;
                price[8] = putR;
                price[9] = putV;
                Program.GUI.BeginInvoke(Program.GUI.myresult);
            };
            Thread a = new Thread(new ParameterizedThreadStart(cal));
            a.Start();
        }

        private void antithetic_CheckedChanged(object sender, EventArgs e)
        {

        }

        private void Stockprice_TextChanged(object sender, EventArgs e)
        {
            double num;
            if (!double.TryParse(Stockprice.Text, out num))
            {
                errorProvider1.SetError(Stockprice, "please enter a positive int number");
            }
            else if (Convert.ToDouble(Stockprice.Text) <= 0)
            {
                errorProvider1.SetError(Stockprice, "please enter a positive int number");
            }
            else
            {
                errorProvider1.SetError(Stockprice, string.Empty);
            }
        }

        private void Kprice_TextChanged(object sender, EventArgs e)
        {
            double num;
            if (!double.TryParse(Kprice.Text, out num))
            {
                errorProvider1.SetError(Kprice, "please enter a positive int number");
            }
            else if (Convert.ToDouble(Kprice.Text) <= 0)
            {
                errorProvider1.SetError(Kprice, "please enter a positive int number");
            }
            else
            {
                errorProvider1.SetError(Kprice, string.Empty);
            }
        }

        private void rate_TextChanged(object sender, EventArgs e)
        {
            double num;
            if (!double.TryParse(rate.Text, out num))
            {
                errorProvider1.SetError(rate, "please enter a positive int number");
            }
            else if (Convert.ToDouble(rate.Text) <= 0)
            {
                errorProvider1.SetError(rate, "please enter a positive int number");
            }
            else
            {
                errorProvider1.SetError(rate, string.Empty);
            }
        }

        private void vol_TextChanged(object sender, EventArgs e)
        {
            double num;
            if (!double.TryParse(vol.Text, out num))
            {
                errorProvider1.SetError(vol, "please enter a positive int number");
            }
            else if (Convert.ToDouble(vol.Text) <= 0)
            {
                errorProvider1.SetError(vol, "please enter a positive int number");
            }
            else
            {
                errorProvider1.SetError(vol, string.Empty);
            }
        }

        private void Tenor_TextChanged(object sender, EventArgs e)
        {
            double num;
            if (!double.TryParse(Tenor .Text, out num))
            {
                errorProvider1.SetError(Tenor, "please enter a positive int number");
            }
            else if (Convert.ToDouble(Tenor.Text) <= 0)
            {
                errorProvider1.SetError(Tenor, "please enter a positive int number");
            }
            else
            {
                errorProvider1.SetError(Tenor, string.Empty);
            }
        }

        private void Trials_TextChanged(object sender, EventArgs e)
        {
            double num;
            if (!double.TryParse(Trials.Text, out num))
            {
                errorProvider1.SetError(Trials, "please enter a positive int number");
            }
            else if (Convert.ToDouble(Trials.Text) <= 0)
            {
                errorProvider1.SetError(Trials, "please enter a positive int number");
            }
            else
            {
                errorProvider1.SetError(Trials, string.Empty);
            }
        }

        private void steps_TextChanged(object sender, EventArgs e)
        {
            double num;
            if (!double.TryParse(steps.Text, out num))
            {
                errorProvider1.SetError(steps, "please enter a positive int number");
            }
            else if (Convert.ToDouble(steps.Text) <= 0)
            {
                errorProvider1.SetError(steps, "please enter a positive int number");
            }
            else
            {
                errorProvider1.SetError(steps, string.Empty);
            }
        }
    }
}
